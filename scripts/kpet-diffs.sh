#!/usr/bin/env bash

dbpath=$1
old_commit=$2

STD_TREES=`kpet --db $dbpath tree list`

function generate_tree
{
  dbpath=$1
  tree_name=$2
  arch=$3
  outpath=$4

  # use random stuff
  ci_pipeline_id=123456
  ci_commit_ref_name=deadbeef

  kpet --db $dbpath run generate -t ${tree_name} -k '##KPKG_URL##' \
   -a ${arch} -o ${outpath} \
   -d "skt@gitlab:${ci_pipeline_id} "'##KVER##'"@${ci_commit_ref_name} ${arch}"

}

function generate_all
{
  dest=$1
  dbpath=$2
  for TREE_NAME in $STD_TREES
  do
    TREE_ARCHES=`kpet --db $dbpath arch list -t $TREE_NAME`
    for ARCH_CONFIG in $TREE_ARCHES
    do
      echo "Generating $dest $ARCH_CONFIG for $TREE_NAME"
      generate_tree $dbpath $TREE_NAME $ARCH_CONFIG ./${dest}-${TREE_NAME}-${ARCH_CONFIG}.xml
    done
  done
}

function diff_all
{
  for TREE_NAME in $STD_TREES
  do
    TREE_ARCHES=`kpet --db $dbpath arch list -t $TREE_NAME`
    for ARCH_CONFIG in $TREE_ARCHES
    do
      diff ./old-${TREE_NAME}-${ARCH_CONFIG}.xml ./new-${TREE_NAME}-${ARCH_CONFIG}.xml > ./diff-${TREE_NAME}-${ARCH_CONFIG}.xml || true &
    done
  done
}

# generate using current kpet-db master and generate XMLs
generate_all new `pwd`/kpet-db
wait

cp -r $dbpath kpet-db-old
# checkout past (before pr) db and generate XMLs
cd kpet-db-old
git reset --hard $old_commit
cd ../
generate_all old `pwd`/kpet-db-old
wait

# run diffs
diff_all
wait

echo "Done"
